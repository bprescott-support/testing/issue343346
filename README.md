# issue343346

fast forward, pipelines, and MRs

# fast-forward workflow

Project contains four protected branches:

- `main`
  - the default branch
  - developers' features branches are merged to `main`
  - often called `development`

- `test`
  - periodically, a MR rolls up commits and merges them fast-forward from `main`

- `pre-prod`
  - merged to from `test`, fast-forward

- `production`
  - merged to from `pre-prod`, fast-forward
